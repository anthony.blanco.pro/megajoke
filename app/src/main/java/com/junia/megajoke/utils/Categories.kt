package com.junia.megajoke.utils

operator fun Boolean.inc() = !this;

data class Categories(
    var programming : Boolean = true,
    var misc : Boolean = false ,
    var dark : Boolean = false,
    var pun : Boolean = false,
    var spooky : Boolean = false,
    var christmas : Boolean = false) {

    override fun toString():String {
        val list = mapOf<String,Boolean>(
            "programming" to programming,
            "misc" to misc,
            "dark" to dark,
            "pun" to pun,
            "spooky" to spooky,
            "christmas" to christmas
        )

        var str = "";
        var index = 0
        list.entries.forEach { (name, value) ->
            if (value) {
                if (index > 0) {
                    str += ","
                }
                str += name;
                index++
            }
        }

        return str;
    }

    fun toggle(name: String) {
        when(name){
            "programming" -> programming++
            "misc" -> misc++
            "dark" -> dark++
            "pun" -> pun++
            "spooky" -> spooky++
            "christmas" -> christmas++
        }
    }

}

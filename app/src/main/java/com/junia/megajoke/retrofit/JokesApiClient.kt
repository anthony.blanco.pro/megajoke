package com.junia.megajoke.retrofit

import com.google.gson.GsonBuilder
import com.junia.megajoke.service.JokesApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object JokesApiClient {

    private var BASE_URL: String = "https://v2.jokeapi.dev/"

    private val gson = GsonBuilder().setLenient().create()
    private val logInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    private val httpClient = OkHttpClient.Builder().addInterceptor(logInterceptor).build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    val service: JokesApiService = retrofit.create(JokesApiService::class.java)
}
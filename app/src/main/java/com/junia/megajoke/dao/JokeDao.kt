package com.junia.megajoke.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.junia.megajoke.entity.Joke

@Dao
interface JokeDao {

    @Query("SELECT * FROM joke")
    fun getAll(): List<Joke>

    @Insert
    fun insert(vararg jokes: Joke)

    @Query("DELETE FROM joke WHERE id = :id")
    suspend fun deleteById(id: Long)

    @Query("DELETE FROM joke")
    fun deleteAll()

    @Delete
    fun delete(joke: Joke)

}
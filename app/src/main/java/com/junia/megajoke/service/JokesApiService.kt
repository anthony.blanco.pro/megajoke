package com.junia.megajoke.service

import com.junia.megajoke.model.Joke
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface JokesApiService {

    @GET("joke/{categories}?type=twopart")
    fun getJoke(@Path("categories") categories: String, @Query("lang") language: String): Call<Joke>

}
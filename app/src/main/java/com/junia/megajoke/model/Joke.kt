package com.junia.megajoke.model

import com.google.gson.annotations.Expose

data class Joke (

    @Expose
    val id: Long,

    @Expose
    val lang: String,

    @Expose
    val error: Boolean,

    @Expose
    val message: String,

    @Expose
    val category: String,

    @Expose
    val flags: JokeFlag,

    @Expose
    val setup: String,

    @Expose
    val delivery: String,

    @Expose
    val safe: Boolean

)
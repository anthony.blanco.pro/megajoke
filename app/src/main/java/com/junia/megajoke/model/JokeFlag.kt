package com.junia.megajoke.model

import com.google.gson.annotations.Expose

class JokeFlag (

    @Expose
    val nsfw: Boolean,

    @Expose
    val religious: Boolean,

    @Expose
    val political: Boolean,

    @Expose
    val racist: Boolean,

    @Expose
    val sexist: Boolean,

    @Expose
    val explicit: Boolean,

)
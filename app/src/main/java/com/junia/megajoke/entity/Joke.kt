package com.junia.megajoke.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Joke (

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,

    @ColumnInfo(name = "type")
    var category: String,

    @ColumnInfo(name = "setup")
    var setup: String,

    @ColumnInfo(name = "delivery")
    var delivery: String,

    @ColumnInfo(name = "safe")
    var safe: Boolean,

    @ColumnInfo(name = "savedAt")
    var savedAt: String

)
package com.junia.megajoke.ui.favorites

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.junia.megajoke.database.AppDatabase
import com.junia.megajoke.entity.Joke

class FavoritesViewModel(application: Application) : AndroidViewModel(application) {

    private val jokeDao = AppDatabase.getDatabase(application.applicationContext).jokeDao()

    private var _favJokes = MutableLiveData<List<Joke>>().apply {
        value = jokeDao.getAll()
    }
    val favJokes = _favJokes

    fun update() {
        favJokes.value = jokeDao.getAll()
        favJokes.value = favJokes.value
    }

    fun removeJoke(index: Int) {
        val item = favJokes.value!![index]
        jokeDao.delete(item)
        favJokes.value = jokeDao.getAll()
        favJokes.value = favJokes.value
    }

}
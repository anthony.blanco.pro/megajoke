package com.junia.megajoke.ui.favorites

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.junia.megajoke.R
import com.junia.megajoke.entity.Joke

class FavoritesAdapter (private val items: List<Joke>, private val listener: OnItemClickListener) : RecyclerView.Adapter<FavoritesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_favorites_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        holder.textDate.text = item.savedAt
        holder.textJoke.text = item.setup
        holder.textAnswer.text = item.delivery
        holder.textSafety.text = if (item.safe) "SFW" else "NSFW"
        holder.textCategory.text = "Category : \"" + item.category + "\""
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView), View.OnClickListener {
        val textJoke: TextView = itemView.findViewById(R.id.textJoke)
        val textAnswer: TextView = itemView.findViewById(R.id.textAnswer)
        val textDate: TextView = itemView.findViewById(R.id.textDate)
        val textSafety: TextView = itemView.findViewById(R.id.textSafety)
        val textCategory: TextView = itemView.findViewById(R.id.textCategories)

        init {
            val buttonDelete: ImageButton = itemView.findViewById(R.id.buttonDelete)
            buttonDelete.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            val position = adapterPosition
            if (adapterPosition != RecyclerView.NO_POSITION) {
                listener.onItemClick(adapterPosition)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

}
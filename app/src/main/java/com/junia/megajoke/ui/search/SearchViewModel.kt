package com.junia.megajoke.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.junia.megajoke.utils.Categories

class SearchViewModel : ViewModel() {
    // Title
    private val _text = MutableLiveData<String>().apply {
        value = "This is Search Fragment"
    }
    val text: LiveData<String> = _text

    // Categories
    private var _categories = MutableLiveData<Categories>().apply {
        value = Categories()
    }

    var categories = _categories

    fun toggleCategory(name: String) {
        categories.value!!.toggle(name);
        categories.value = categories!!.value
    }

}
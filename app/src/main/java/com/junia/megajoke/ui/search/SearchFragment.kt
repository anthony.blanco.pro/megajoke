package com.junia.megajoke.ui.search

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.junia.megajoke.MainActivity
import com.junia.megajoke.R
import com.junia.megajoke.database.AppDatabase

import com.junia.megajoke.databinding.FragmentSearchBinding
import com.junia.megajoke.model.Joke
import com.junia.megajoke.retrofit.JokesApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap

class SearchFragment : Fragment() {

    private var _binding: FragmentSearchBinding? = null
    private var currentJoke :Joke? = null

    private val binding get() = _binding!!
    private lateinit var searchViewModel : SearchViewModel;

    private fun getColor(value:Boolean) : ColorStateList {
        return requireActivity().getColorStateList(if(value) R.color.btn_active else R.color.btn_inactive)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        searchViewModel =
            ViewModelProvider(this).get(SearchViewModel::class.java)

        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val button: Button = binding.searchBtn

        button.setOnClickListener { _ -> showJoke() }

        val categoryBtns = HashMap<String,Button>()
        categoryBtns["programming"] = binding.btnProgramming
        categoryBtns["misc"] = binding.btnMisc
        categoryBtns["dark"] = binding.btnDark
        categoryBtns["pun"] = binding.btnPun
        categoryBtns["spooky"] = binding.btnSpooky
        categoryBtns["christmas"] = binding.btnChristmas

        categoryBtns.forEach{(name, btn) ->
            btn.setOnClickListener {_ -> searchViewModel.toggleCategory(name)}
        }

        searchViewModel.categories.observe(viewLifecycleOwner) {
            val categories = it!!

            categoryBtns["programming"]!!.setBackgroundTintList(getColor(categories.programming))
            categoryBtns["misc"]!!.setBackgroundTintList(getColor(categories.misc))
            categoryBtns["dark"]!!.setBackgroundTintList(getColor(categories.dark))
            categoryBtns["pun"]!!.setBackgroundTintList(getColor(categories.pun))
            categoryBtns["spooky"]!!.setBackgroundTintList(getColor(categories.spooky))
            categoryBtns["christmas"]!!.setBackgroundTintList(getColor(categories.christmas))
        }

        binding.favBtn.setOnClickListener {
            if ( binding.favBtn.visibility == View.VISIBLE) {
                val jokeDao = AppDatabase.getDatabase(this.requireContext()).jokeDao()
                jokeDao.insert(com.junia.megajoke.entity.Joke(
                    category = currentJoke!!.category,
                    delivery = currentJoke!!.delivery,
                    setup = currentJoke!!.setup ,
                    safe = currentJoke!!.safe,
                    savedAt = Date().toString() ))

                Toast.makeText(activity,"Successfully saved !", Toast.LENGTH_LONG).show()
                val act = activity as MainActivity
                act.binding.navView.selectedItemId = R.id.navigation_favorites
            }
        }

        return root
    }

    private fun showJoke() {
        val call : Call<Joke> = JokesApiClient.service.getJoke(searchViewModel.categories.value.toString(),"EN")

        call.enqueue(object : Callback<Joke> {
            override fun onResponse(call: Call<Joke>, response: Response<Joke>) {
                val body = response.body()
                currentJoke = body

                if (body!!.error){
                    binding.textViewSetup.text = "Error"
                    binding.textViewDelivery.text = body!!.message
                    binding.favBtn.visibility = View.INVISIBLE;
                } else {
                    binding.textViewSetup.text = body!!.setup
                    binding.textViewDelivery.text = body!!.delivery
                    binding.favBtn.visibility = View.VISIBLE;
                }


            }

            override fun onFailure(call: Call<Joke>, t: Throwable) {
                binding.textViewSetup.text = "Error"
                binding.textViewDelivery.text = "During api call"
                binding.favBtn.visibility = View.INVISIBLE;
            }
        }
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}